#!/bin/bash
#SBATCH --job-name=seqkit # job name (-J)
#SBATCH --time="48:00:00" #max run time "hh:mm:ss" or "dd-hh:mm:ss" (-t)
#SBATCH --cpus-per-task=1 # max nb of cores (-c)
#SBATCH --ntasks=1 #nb of tasks
#SBATCH --mem=30G # max memory (-m)
#SBATCH --output=seqkit.out #stdout (-o)

########################## On genotoul ###############################
# Uncomment the module load for genotoul
## snakemake 5.3
#module load system/Python-3.6.3
## for singularity
module load system/singularity-3.5.3
######################################################################


export SINGULARITY_BINDPATH="/work2/project/gafl"


mygenom="/work/project/gafl/Data/Solanaceae/Capsicum_annuum/DNA/Ref_Genome/Korea_CM334/Annuum.CM334.v1.6.chr00_12chrs.fa"
seqkit="singularity exec /work/project/gafl/tools/containers/seqkit_v0.12.0.sif seqkit"


cat $mygenom| $seqkit fx2tab  > genome_CM334_table.csv # export fasta to table (column 1: definition, column 2: sequence)



while read def seq 
do
   def1=$(echo $def|sed 's/ +/_/g') # def1 is the sequence definition without space 
   echo $seq| sed -r 's/[nN]{1000,}/\n/gi' \ # replace in sequence 1000 or more "n" or "N" by newline
   | cat -n \ # had a number to rows
   | ${seqkit} tab2fx \ # convert to fasta format
   | ${seqkit} replace -p "(.+)" -r "Contig{nr} $def1" >> CM334_v1.6_cut.fa # change names of sequences definitions

done < genome_CM334_table.csv # input data: fasta in table format


exit 0






